create table IF NOT EXISTS users_dev (
    id int(11) unsigned not null auto_increment,
    name varchar(75) not null,
    password varchar(255) not null,
    email varchar(100) not null,
    primary key (id),
    unique key email (email)
)